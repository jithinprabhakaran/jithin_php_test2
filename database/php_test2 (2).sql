-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 20, 2017 at 11:21 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_test2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', 'admin', 'admin'),
(2, 'jithin', 'jithin', 'judge'),
(3, 'hari', 'hari', 'judge'),
(4, 'shabana', 'shabana', 'judge');

-- --------------------------------------------------------

--
-- Table structure for table `cactegory`
--

CREATE TABLE `cactegory` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cactegory`
--

INSERT INTO `cactegory` (`id`, `name`) VALUES
(1, 'off stage events'),
(2, 'on stage events');

-- --------------------------------------------------------

--
-- Table structure for table `competition`
--

CREATE TABLE `competition` (
  `id` int(10) NOT NULL,
  `c_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `max_mark` int(10) NOT NULL,
  `weightage` int(10) NOT NULL,
  `time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `competition`
--

INSERT INTO `competition` (`id`, `c_id`, `name`, `max_mark`, `weightage`, `time`) VALUES
(1, 1, 'drawing', 50, 5, 30),
(2, 1, 'writing', 50, 10, 30),
(3, 2, 'dance', 50, 10, 5),
(4, 2, 'drama', 50, 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `judges`
--

CREATE TABLE `judges` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `compt_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `judges`
--

INSERT INTO `judges` (`id`, `name`, `compt_id`) VALUES
(1, 'jithin', 1),
(2, 'hari', 1),
(3, 'shabana', 2),
(4, 'jeslin', 2),
(5, 'sudhi', 3),
(6, 'glen', 3);

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `id` int(10) NOT NULL,
  `compet_id` int(10) NOT NULL,
  `p_id` int(10) NOT NULL,
  `j_id` int(10) NOT NULL,
  `mark` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`id`, `compet_id`, `p_id`, `j_id`, `mark`) VALUES
(3, 2, 18, 3, 33),
(4, 1, 26, 2, 10),
(8, 1, 26, 1, 44),
(9, 1, 11, 1, 33),
(11, 1, 11, 2, 11),
(12, 1, 18, 1, 22),
(13, 1, 18, 1, 22),
(14, 1, 18, 1, 22),
(15, 1, 18, 1, 22),
(16, 3, 17, 5, 55),
(17, 3, 17, 6, 44),
(18, 3, 18, 5, 43),
(19, 3, 18, 6, 33),
(20, 3, 26, 5, 55),
(21, 3, 26, 6, 58),
(22, 2, 16, 3, 36),
(23, 2, 16, 4, 37),
(24, 2, 19, 3, 64),
(25, 2, 19, 4, 67),
(26, 3, 13, 5, 66),
(27, 3, 17, 6, 33),
(28, 2, 20, 3, 44),
(29, 1, 11, 1, 55),
(30, 1, 11, 2, 22),
(31, 1, 11, 2, 22),
(32, 1, 11, 2, 22),
(33, 1, 11, 2, 22),
(34, 1, 25, 1, 43),
(35, 1, 25, 2, 34),
(36, 1, 25, 2, 34),
(37, 2, 28, 3, 56),
(38, 2, 28, 4, 54),
(39, 2, 28, 4, 54),
(40, 2, 26, 3, 11),
(41, 2, 26, 4, 12),
(42, 1, 21, 3, 44),
(43, 1, 21, 3, 44),
(44, 1, 21, 3, 33),
(45, 1, 15, 3, 34),
(46, 1, 15, 3, 35),
(47, 1, 30, 4, 43),
(48, 1, 30, 4, 43),
(49, 1, 30, 3, 42),
(50, 1, 30, 2, 35),
(51, 3, 30, 2, 45),
(52, 3, 29, 2, 44),
(53, 3, 29, 2, 44),
(54, 2, 23, 3, 55),
(55, 2, 23, 3, 55),
(56, 2, 23, 3, 55),
(57, 2, 23, 3, 55),
(58, 2, 23, 3, 55),
(59, 2, 27, 3, 77),
(60, 3, 26, 2, 33),
(61, 3, 9, 3, 44);

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `school` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `name`, `email`, `school`) VALUES
(1, 'jithin', 'jithin.rbgxc.zyxware@gmail.com', 'abcd'),
(2, 'jithin', 'jithin.rbgxc.zyxware@gmail.com', 'abcd'),
(3, 'gh', 'gh@er', 'gjjgj'),
(4, 'gh', 'gh@er', 'gjjgj'),
(5, 'gh', 'gh@er', 'gjjgj'),
(6, 'gh', 'gh@er', 'gjjgj'),
(7, 'gh', 'gh@er', 'gjjgj'),
(8, 'j bk,', ' gj', 'gj'),
(9, '5t', 'tuf', 'yfu'),
(10, 'ytuty', 'yut', 'tyty'),
(11, 'jeslin', 'dfgfg', 'dxb '),
(12, 'jeslin', 'dfgfg', 'dxb '),
(13, 'tytch', 'tfuhc', 'ty'),
(14, '879y8', 'y9', '8y9i'),
(15, 'kj,j', 'kjljkk', 'jk'),
(16, 'iho', 'uh', 'ygi'),
(17, 'hari', 'fgcb', 'gn'),
(18, 'shab', 'fdd', 'hjng'),
(19, 'kl;', 'kj', 'jil'),
(20, 'ghng', 'hdf@fg', 'jh'),
(21, 'jhkj', 'jhk', 'hjkh'),
(22, 'dg', 'gdr', 'dg'),
(23, 'tfg', 'htc', 'jnhgc'),
(24, 'sudhish', 'hmnjhgjg', 'jhkhkhk'),
(25, 'sudhish', 'g', 'ghfh'),
(26, 'abhi', 'gfhgfh@wew', 'gj'),
(27, 'gh', 'fgcb', 'dg'),
(28, 'qwerty', 'h@fhh', 'abcd'),
(29, 'qwerty', 'h@fhh', 'gj'),
(30, 'sumesh', 'sum', 'fgd');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` int(10) NOT NULL,
  `chestnum` int(10) NOT NULL,
  `p_id` int(10) NOT NULL,
  `comp_id` int(10) NOT NULL,
  `tot_mark` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `chestnum`, `p_id`, `comp_id`, `tot_mark`) VALUES
(1, 550, 8, 1, 0),
(2, 770, 9, 3, 44),
(6, 997, 10, 4, 0),
(7, 506, 11, 1, 26.7143),
(8, 1000, 13, 3, 66),
(9, 599, 13, 4, 0),
(10, 157, 14, 4, 0),
(11, 121, 15, 1, 34.5),
(12, 711, 16, 2, 36),
(13, 989, 17, 4, 0),
(14, 984, 17, 3, 44),
(19, 710, 19, 2, 65.5),
(21, 441, 19, 3, 0),
(26, 165, 20, 2, 44),
(27, 639, 20, 4, 0),
(28, 891, 21, 4, 0),
(29, 574, 21, 1, 40.3333),
(30, 573, 23, 2, 55),
(31, 956, 23, 4, 0),
(32, 809, 25, 1, 37),
(33, 575, 26, 1, 0),
(34, 948, 26, 2, 11.5),
(35, 533, 26, 4, 0),
(36, 326, 26, 3, 48.6667),
(37, 970, 27, 2, 77),
(38, 725, 27, 1, 0),
(39, 467, 27, 4, 0),
(40, 391, 27, 3, 0),
(41, 716, 28, 2, 54.6667),
(42, 862, 28, 1, 0),
(43, 996, 28, 4, 0),
(44, 698, 28, 3, 0),
(45, 183, 29, 1, 0),
(46, 389, 29, 2, 0),
(47, 648, 29, 4, 0),
(48, 586, 29, 3, 44),
(49, 822, 30, 1, 40.75),
(50, 302, 30, 2, 0),
(51, 131, 30, 4, 0),
(52, 477, 30, 3, 45);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cactegory`
--
ALTER TABLE `cactegory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `competition`
--
ALTER TABLE `competition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `judges`
--
ALTER TABLE `judges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cactegory`
--
ALTER TABLE `cactegory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `competition`
--
ALTER TABLE `competition`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `judges`
--
ALTER TABLE `judges`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
