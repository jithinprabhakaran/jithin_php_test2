<?php 
require_once 'controllers.php';
require_once 'model.php';
session_start();
$uri = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
if ("/" == $uri) {
   show_reg_page();
}
elseif("/index.php/insert/" == $uri) {
    insert_action();    
}
elseif("/index.php/showprogram" == $uri) {
    show_programs();
}
elseif("/index.php/program_reg" == $uri) {
    reg_program();
}
elseif("/index.php/logout/" == $uri) {
    logout();
}
elseif("/index.php/judge_login/" == $uri) {
    check_login();
}
elseif("/index.php/judge/" == $uri) {
    show_login();
}
elseif("/index.php/showadminprogram" == $uri) {
    show_admin_prg();
}
elseif("/index.php/show_participant" == $uri) {
    show_particpant();
}
elseif("/index.php/addmark" == $uri) {
    show_form();
}
elseif("/index.php/insertmarks" == $uri) {
    show_judgemark();
}
elseif("/index.php/judge_mark_insert" == $uri) {
    insert_judge_mark();   
}
elseif("/index.php/adminlogout/" == $uri) {
    session_destroy();
    show_reg_page();  
}
elseif("/index.php/overall/" == $uri) {
    overall_result();  
}


?>
