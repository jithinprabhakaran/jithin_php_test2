<?php
function open_database_connection() {
    $link = new PDO ("mysql:host=localhost;dbname=php_test2",'root','root');
    return $link;
}
function close_database_connection( $link ) {
    $link = null;
}
function insert_part() {
    $link = open_database_connection();
    $stmt = $link-> prepare("INSERT INTO participant( name, email, school ) VALUES (:name, :email, :school)");
    $stmt->bindParam(":name",$_POST['name']);
    $stmt->bindParam(":email",$_POST['email']);
    $stmt->bindParam(":school",$_POST['school']);
    $t = $stmt->execute();
    $lid = $link->lastInsertId();
    close_database_connection( $link );
    return $lid;        
}
function select_cact() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT * FROM cactegory");
    $t = $stmt->execute();
    while($cat = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $cactegory[] = $cat;
    }
    close_database_connection( $link );
    return $cactegory;
}
function select_program() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT * FROM competition WHERE c_id = :cid");
    $stmt->bindParam(":cid",$_GET['cid']);
    $t = $stmt->execute();
    while($prog = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $programs[] = $prog;
    }
    close_database_connection( $link );
    return $programs;
}
function insert_part_program() {
    $link = open_database_connection();
    $chestno = rand(100,1000);
        $mark = 0;
        $stmt = $link-> prepare("INSERT INTO registration( chestnum, p_id, comp_id, tot_mark ) VALUES (:chestnum, :p_id, :comp_id, :tot_mark)");
        $stmt->bindParam(":chestnum",$chestno);
        $stmt->bindParam(":p_id",$_GET['p_id']);
        $stmt->bindParam(":comp_id",$_GET['pro_id']);
        $stmt->bindParam(":tot_mark",$mark);
        $t = $stmt->execute();
        close_database_connection( $link );
}
function login_admin() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT * FROM admin WHERE username = :username AND password = :pass");
    $stmt->bindParam(":username",$_POST['name']);
    $stmt->bindParam(":pass",$_POST['password']);
    $t = $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    close_database_connection( $link );
    return $row;
}
function list_participant() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT p.id, p.name, r.chestnum, r.tot_mark FROM participant p JOIN registration r ON p.id=r.p_id WHERE comp_id = :cid ORDER BY r.tot_mark DESC");
    $stmt->bindParam(":cid",$_GET['pro_id']);
    $t = $stmt->execute();
    while($part = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $participants[] = $part;
    }
    close_database_connection( $link );
    return $participants;
}
function show_mark_form() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT id, name FROM judges WHERE compt_id = :compt_id");
    $stmt->bindParam(":compt_id",$_GET['prog_id']);
    $t = $stmt->execute();
    while($judg = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $judges[] = $judg;
    }
    close_database_connection( $link );
    return $judges;  
}
function judgement_mark() {
    $link = open_database_connection();
    $stmt = $link-> prepare("INSERT INTO marks( compet_id, p_id, j_id, mark ) VALUES (:compet_id, :p_id, :j_id, :mark)");
    $stmt->bindParam(":compet_id",$_GET['pro_id']);
    $stmt->bindParam(":p_id",$_GET['p_id']);
    $stmt->bindParam(":j_id",$_GET['j_id']);
    $stmt->bindParam(":mark",$_POST['marks']);
    $t = $stmt->execute();
    close_database_connection( $link ); 
}
function sum_mark() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT AVG(mark) as avg FROM marks WHERE compet_id = :compet_id AND p_id = :p_id");
    $stmt->bindParam(":compet_id",$_GET['pro_id']);
    $stmt->bindParam(":p_id",$_GET['p_id']);
    $t = $stmt->execute();
    $avg = $stmt->fetch(PDO::FETCH_ASSOC); 
    close_database_connection( $link );
    return $avg;  
}
function update_tot_mark($avg) {
    $link = open_database_connection();
    $stmt = $link-> prepare("UPDATE registration SET tot_mark = :tot_mark WHERE comp_id = :comp_id AND p_id = :p_id");
    $stmt->bindParam(":tot_mark",$avg['avg']);
    $stmt->bindParam(":comp_id",$_GET['pro_id']);
    $stmt->bindParam(":p_id",$_GET['p_id']);  
    $t = $stmt->execute();
    close_database_connection( $link ); 
}
function fetch_overall() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT r.p_id, p.name, (SUM(r.tot_mark*c.weightage))/100 FROM registration r JOIN competition c ON c.id=r.comp_id JOIN participant p ON p.id = r.p_id GROUP BY p_id ORDER BY (SUM(r.tot_mark*c.weightage))/100 DESC");
    $t = $stmt->execute();
    while($overall = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $overallresults[] = $overall;
    }
    close_database_connection( $link );
    return $overallresults;  
}