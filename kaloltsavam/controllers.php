<?php
function show_reg_page() {
    include 'templates/reg.tpl.php';
}
function insert_action() {
    $l_id = insert_part();
    $cactegory = select_cact();
    include 'templates/cactegory.tpl.php';
}
function show_programs() {
    $programs = select_program();
    include 'templates/programs.tpl.php';
}
function reg_program() {
    insert_part_program();
    $l_id = $_GET['p_id'];
    $cactegory = select_cact();
    include 'templates/cactegory.tpl.php';
}
function logout() {
    show_reg_page();
}
function check_login() {
    $row = login_admin();
    if ( $row['id'] == NULL ) {
        header("Location:http://phptest2.local/index.php/judge/");
    }
    else {
        $_SESSION['jid'] = $row['id'];
        $cactegory = select_cact();
        include 'templates/admincactegory.tpl.php';
    }
}
function show_login() {
    include 'templates/login.tpl.php'; 
}
function show_admin_prg() {
    $programs = select_program();
    include 'templates/adminprograms.tpl.php';
}
function show_particpant() {
    $participants = list_participant();
    include 'templates/participants.tpl.php';

}
function  show_judgemark() {
    include 'templates/judgeform.tpl.php';  
}
function insert_judge_mark() {
    judgement_mark();
    $avg = sum_mark();
    update_tot_mark($avg);
    show_particpant();

}
function overall_result() {
    $overallresults = fetch_overall();
    include 'templates/overall.tpl.php'; 
}
